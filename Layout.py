import pickle as pkl

import re
import sys
sys.path.append('/sf/bd/applications/OnlineModel/current') 
#!/usr/bin/env python
from OMAppTemplate import ApplicationTemplate
from OMFacility import *

class Layout(ApplicationTemplate):    
# any calculation for the explicit layout of the machine    

    def __init__(self):

        # get layout
        SF=Facility()
        # 

        self.sections=[]
        self.quads={}
        self.bpms={}
        self.cors={}
        SF.writeFacility(self)

        self.path='/sf/data/applications/BD-QuadAlignment'
        self.loadPickle()
        self.quadparameter['new']={'BPM':'','Cor':'','dQ':'0.0','dCor':'0.0','Orbit':[]}

    def old(self):

        self.quadparameter['SARCL01-MQUA020']={'BPM':'SARCL01-DBPM010','Cor':'S30CB15-MCOR430','dQ':'0.05','dCor':'1','Orbit':['SARCL01-DBPM060','SARCL01-DBPM120','SARCL01-DBPM150','SARCL02-DBPM110','SARCL02-DBPM220']}
#        self.quadparameter['SARCL01-MQUA050']={'BPM':'SARCL01-DBPM060','Cor':'S30CB15-MCOR430','dQ':'0.05','dCor':'1','Orbit':['SARCL01-DBPM120','SARCL01-DBPM150','SARCL02-DBPM110','SARCL02-DBPM220','SARCL02-DBPM260']}
        self.quadparameter['SARCL01-MQUA080']={'BPM':'SARCL01-DBPM060','Cor':'SARCL01-MCOR020','dQ':'0.05','dCor':'1','Orbit':['SARCL01-DBPM120','SARCL01-DBPM150','SARCL02-DBPM110','SARCL02-DBPM220','SARCL02-DBPM260']}
        self.quadparameter['SARCL01-MQUA100']={'BPM':'SARCL01-DBPM120','Cor':'SARCL01-MCOR020','dQ':'0.05','dCor':'1','Orbit':['SARCL01-DBPM150','SARCL02-DBPM110','SARCL02-DBPM220','SARCL02-DBPM260','SARCL02-DBPM330']}
#        self.quadparameter['SARCL01-MQUA140']={'BPM':'SARCL01-DBPM150','Cor':'SARCL01-MCOR040','dQ':'0.05','dCor':'1','Orbit':['SARCL02-DBPM110','SARCL02-DBPM220','SARCL02-DBPM260','SARCL02-DBPM330','SARCL02-DBPM470']}
        self.quadparameter['SARCL01-MQUA190']={'BPM':'SARCL01-DBPM150','Cor':'SARCL01-MCOR040','dQ':'0.05','dCor':'1','Orbit':['SARCL02-DBPM110','SARCL02-DBPM220','SARCL02-DBPM260','SARCL02-DBPM330','SARCL02-DBPM470']}
        self.quadparameter['SARCL02-MQUA130']={'BPM':'SARCL02-DBPM110','Cor':'SARCL01-MCOR130','dQ':'0.05','dCor':'1','Orbit':['SARCL02-DBPM220','SARCL02-DBPM260','SARCL02-DBPM330','SARCL02-DBPM470','SARMA01-DBPM040']}
        self.quadparameter['SARCL02-MQUA210']={'BPM':'SARCL02-DBPM220','Cor':'SARCL01-MCOR180','dQ':'0.05','dCor':'1','Orbit':['SARCL02-DBPM260','SARCL02-DBPM330','SARCL02-DBPM470','SARMA01-DBPM040','SARMA01-DBPM100']}
        self.quadparameter['SARCL02-MQUA250']={'BPM':'SARCL02-DBPM260','Cor':'SARCL02-MCOR120','dQ':'0.03','dCor':'2','Orbit':['SARCL02-DBPM330','SARCL02-DBPM470','SARMA01-DBPM040','SARMA01-DBPM100','SARMA02-DBPM010']}
#        self.quadparameter['SARCL02-MQUA310']={'BPM':'SARCL02-DBPM330','Cor':'SARCL02-MCOR240','dQ':'0.03','dCor':'2','Orbit':['SARCL02-DBPM470','SARMA01-DBPM040','SARMA01-DBPM100','SARMA02-DBPM010','SARMA02-DBPM020']}
        self.quadparameter['SARCL02-MQUA350']={'BPM':'SARCL02-DBPM330','Cor':'SARCL02-MCOR240','dQ':'0.05','dCor':'2','Orbit':['SARCL02-DBPM470','SARMA01-DBPM040','SARMA01-DBPM100','SARMA02-DBPM010','SARMA02-DBPM020']}
        self.quadparameter['SARCL02-MQUA460']={'BPM':'SARCL02-DBPM470','Cor':'SARCL02-MCOR340','dQ':'0.05','dCor':'2','Orbit':['SARMA01-DBPM040','SARMA01-DBPM100','SARMA02-DBPM010','SARMA02-DBPM020','SARMA02-DBPM040']}
        self.quadparameter['SARMA01-MQUA060']={'BPM':'SARMA01-DBPM040','Cor':'SARCL02-MCOR480','dQ':'0.01','dCor':'1','Orbit':['SARMA01-DBPM100','SARMA02-DBPM010','SARMA02-DBPM020','SARMA02-DBPM040','SARMA02-DBPM110']}
        self.quadparameter['SARMA01-MQUA120']={'BPM':'SARMA01-DBPM100','Cor':'SARMA01-MCOR020','dQ':'0.02','dCor':'2','Orbit':['SARMA02-DBPM010','SARMA02-DBPM020','SARMA02-DBPM040','SARMA02-DBPM110','SARUN01-DBPM070']}
        self.quadparameter['SARMA01-MQUA140']={'BPM':'SARMA02-DBPM010','Cor':'SARMA01-MCOR070','dQ':'0.02','dCor':'1','Orbit':['SARMA02-DBPM020','SARMA02-DBPM040','SARMA02-DBPM110','SARUN01-DBPM070','SARUN02-DBPM070']}
        self.quadparameter['SARMA02-MQUA050']={'BPM':'SARMA02-DBPM040','Cor':'SARMA01-MCOR130','dQ':'0.02','dCor':'1','Orbit':['SARMA02-DBPM110','SARUN01-DBPM070','SARUN02-DBPM070','SARUN03-DBPM070','SARUN04-DBPM070']}
        self.quadparameter['SARMA02-MQUA120']={'BPM':'SARMA02-DBPM110','Cor':'SARMA01-MCOR050','dQ':'0.02','dCor':'1','Orbit':['SARUN01-DBPM070','SARUN02-DBPM070','SARUN03-DBPM070','SARUN04-DBPM070','SARUN05-DBPM070']}
#        self.savePickle()

        
    def savePickle(self):
        filename=self.path+'/config.pickle'
        fid=open(filename,'wb')
        pkl.dump(self.quadparameter,fid)
        fid.close()

    def loadPickle(self):
        filename=self.path+'/config.pickle'
        fid=open(filename,'rb')
        self.quadparameter=pkl.load(fid)
        fid.close()

    def newEntry(self):
        return {'BPM':'','Cor':'','dQ':'0.1','dCor':'1.0','Orbit':[]}


    def getBPMlist(self,quad):
        sec=quad[0:7]
        i=-1
        for ele in self.sections:
            if sec == ele:
                break
            else:
                i=i+1
        if i < 0:
            i = 0
        res=[]

        for ele in self.bpms[self.sections[i]]:
            res.append(self.sections[i]+'-'+ele)
        for ele in self.bpms[self.sections[i+1]]:
            res.append(self.sections[i+1]+'-'+ele)
        icount=0
        for ele in self.sections[i+2:]:
            for ele2 in self.bpms[ele]:
                res.append(ele+'-'+ele2)
                icount+=1
            if icount > 10:
                break
        return res


    def getCorlist(self,quad):
        sec=quad[0:7]
        i=-1
        for ele in self.sections:
            if sec == ele:
                break
            else:
                i=i+1
        if i < 0:
            i = 0
        res=[]
        for ele in self.cors[self.sections[i]]:
            res.append(self.sections[i]+'-'+ele)
        for ele in self.cors[self.sections[i+1]]:
            res.append(self.sections[i+1]+'-'+ele)
        return res 
    
#----------------------------------------
# specific elements

    def isType(self,name):
        if (name.find('holylist')>-1):
            return 1
        else:
            return 0

    def writeLine(self,ele,seq):
        if len(ele.Name) == 7:
            if not ele.Name in self.sections:
                self.sections.append(ele.Name)
                self.quads[ele.Name]=[]
                self.bpms[ele.Name]=[]
                self.cors[ele.Name]=[]
        return


    def writeDiagnostic(self,ele):
        if not 'BPM' in ele.Name:
            return
        sec=ele.Name[0:7]
        bpm=ele.Name[8:15]
        if sec in self.bpms.keys():
            if bpm not in self.bpms[sec]:
                self.bpms[sec].append(bpm)

    def writeCorrector(self,ele):
        sec=ele.Name[0:7]
        cor=ele.Name[8:15]
        if sec in self.cors.keys():
            if cor not in self.cors[sec]:
                self.cors[sec].append(cor)


    def writeQuadrupole(self,ele):
        sec=ele.Name[0:7]
        quad=ele.Name[8:15]
        if 'MQUP' in quad:
            return
        if sec in self.quads.keys():
            if quad not in self.quads[sec]:
                self.quads[sec].append(quad)
        if 'corx' in ele.__dict__.keys():
            if sec in self.cors.keys():
                if quad not in self.cors[sec]:
                    self.cors[sec].append(quad.replace('QUA','COR'))
            
