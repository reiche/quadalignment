import sys
import datetime
import os.path
import time
import subprocess
from os import walk
import numpy as np
import sys
import re

from PyQt5 import QtGui,QtCore,QtWidgets
from PyQt5.uic import loadUiType


from matplotlib.figure import Figure
import matplotlib.patches as patches

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)

Ui_QuadAlignmentGUI, QMainWindow = loadUiType('QuadAlignment.ui')

# PV interface

from epics import PV,caput,caget
from Layout import *
from QuadWorker import *

# Online Model specific, should become obsolete in the future.
sys.path.append('/sf/bd/applications/OnlineModel/current') 

class QuadAlignment(QMainWindow,Ui_QuadAlignmentGUI):

    signalStatus = QtCore.pyqtSignal(bool)

    def __init__(self,):
        super(QuadAlignment,self).__init__()
        self.setupUi(self)
        self.initmpl()

        self.layout=Layout()
        for ele in self.layout.sections:
            self.SelSection.addItem(ele)
        self.updateSection()
        self.updateQuadrupole()


        # new section is selected
        self.SelSection.currentIndexChanged.connect(self.updateSection)
        self.SelQuad.currentIndexChanged.connect(self.updateQuadrupole)


        # configuration event handler
        self.ConfRemoveBPM.clicked.connect(self.removeBPM)
        self.ConfAddCor.clicked.connect(self.addCorrector)
        self.ConfAddBPMRef.clicked.connect(self.addBPMRef)
        self.ConfAddBPMOrbit.clicked.connect(self.addBPMOrbit)
        self.SaveConfig.clicked.connect(self.saveconfig)


        # measurement stuff

        self.worker = QuadWorker()
        self.worker.signalEnd.connect(self.handle_end)    
        self.worker.signalProgress.connect(self.handle_progress)    
        self.worker.signalPlot.connect(self.handle_plot)    
        self.ButMeasure.clicked.connect(self.measure)
        self.ButAbort.clicked.connect(self.abort)

        self.SetXOFFS.clicked.connect(self.setX)
        self.SetYOFFS.clicked.connect(self.setY)
        self.DoLogBook.clicked.connect(self.makelog)
        self.DoLogBook.setEnabled(False)

    #-----------------------------
    # running the measurement - QThread class
    


    def initMeasurement(self):        
        quad=self.getQuad()
        if not quad in self.layout.quadparameter.keys():
            self.Log.insertPlainText('Measurement not defined')
            return False
        dosave=self.noSave.isChecked()

        bpm=self.layout.quadparameter[quad]['BPM']
        cor=self.layout.quadparameter[quad]['Cor']
        orbit=self.layout.quadparameter[quad]['Orbit']
        dk1L=float(self.layout.quadparameter[quad]['dQ'])
        dCor=float(self.layout.quadparameter[quad]['dCor'])
        
        self.Log.insertPlainText('Scan for Quadrupole %s\n' % quad)
        self.Log.insertPlainText('Field Change (k1L): %s\n' %dk1L)
        self.Log.insertPlainText('Reference BPM: %s\n' % bpm)
        self.Log.insertPlainText('Reference Corrector: %s\n' % cor)
        self.Log.insertPlainText('Corrector Change: %s\n' % dCor)
        self.Log.insertPlainText('BPMs for Orbit Measurement\n')
        for ele in orbit:
            self.Log.insertPlainText('    %s\n' %ele)
        return self.worker.initMeasurement(quad,bpm,cor,orbit,dk1L,dCor,dosave)


    def measure(self):
        if self.worker.isRunning():
            return
        self.DoLogBook.setEnabled(False)
        self.Log.clear()
        status=self.initMeasurement()
        if status:
            self.ButMeasure.setEnabled(False)
            self.ButAbort.setEnabled(True)
            self.worker.start()


    def abort(self):
        self.worker.doAbort()
        self.ButMeasure.setEnabled(True)
        self.ButAbort.setEnabled(False)


    @QtCore.Slot(str)
    def handle_progress(self,e):
        self.msg=e
        self.Log.insertPlainText('%s\n' % e)
        self.Log.moveCursor(QtGui.QTextCursor.End)

    @QtCore.Slot(bool)
    def handle_plot(self,e):
        if not e:
            return

        # plot intermediat steps
        self.axes.clear()
        color=['b','b','r','r','g','g','c','c','y','y']
        mark=['o','s']
        idata=0
        xfit=np.linspace(np.min(self.worker.xdata),np.max(self.worker.xdata),num=10)
        for data in self.worker.ydata:
            pf0=self.worker.slope[self.worker.istep-1,idata,0]
            pf1=self.worker.slope[self.worker.istep-1,idata,1]
            yfit=xfit*pf0+pf1
            clr=color[idata % 10]
            mkr=mark[idata % 2]           
            self.axes.scatter(self.worker.xdata,data,c=clr,marker=mkr,label=self.worker.sensorlist[idata+3])
            self.axes.plot(xfit,yfit,clr)
            idata+=1
        self.axes.set_title(self.msg)
        self.axes.set_xlabel('k1L')
        self.axes.set_ylabel('BPM-Reading')
        self.axes.legend()
        self.axes.grid()
        if self.worker.save:
            fn=self.worker.filename.split('.')
            filename='%s_Fig%d.png' % (fn[0],self.worker.istep)
            self.fig.savefig(filename)
        self.canvas.draw()



    @QtCore.Slot(bool)
    def handle_end(self,t):
        self.ButMeasure.setEnabled(True)
        self.ButAbort.setEnabled(False)
        if (not t) :
            self.DoLogBook.setEnabled(True)
            self.Log.insertPlainText('Measurement ended.')
            self.Log.moveCursor(QtGui.QTextCursor.End)
            # plot result
            color=['b','r','g','c','y']
            self.axes.clear()
            i=0
            xplotfit=np.linspace(np.min(self.worker.xxscan),np.max(self.worker.xxscan),num=5)
            for ydat in self.worker.xyscan:
                clr=color[i % 5]
                self.axes.scatter(self.worker.xxscan,ydat,c=clr,marker='o',label=self.worker.sensorlist[2*i+3])
                yplotfit=self.worker.xfit[i,0]*xplotfit+self.worker.xfit[i,1]
                self.axes.plot(xplotfit,yplotfit,clr)
                i+=1
            i=0
            xplotfit=np.linspace(np.min(self.worker.yxscan),np.max(self.worker.yxscan),num=5)
            for ydat in self.worker.yyscan:
                clr=color[i % 5]
                self.axes.scatter(self.worker.yxscan,ydat,c=clr,marker='s',label=self.worker.sensorlist[2*i+4])
                yplotfit=self.worker.yfit[i,0]*xplotfit+self.worker.yfit[i,1]
                self.axes.plot(xplotfit,yplotfit,clr)
                i+=1
            self.axes.legend()
            self.axes.set_title('Orbit Steering for %s - Offsets (%f, %f)' % (self.getQuad(),self.worker.xoff,self.worker.yoff))
            self.axes.set_xlabel('%s, %s' % (self.worker.sensorlist[0],self.worker.sensorlist[1]))
            self.axes.set_ylabel('Orbit Steering: dx/dk1l, dy/dk1l')
            self.axes.grid()
            if self.worker.save:
                fn=self.worker.filename.split('.')
                filename='%s_Fig6.png' % (fn[0])
                self.fig.savefig(filename)
            self.canvas.draw()
            self.XOFFS.setText('%f' % self.worker.xoff)
            self.YOFFS.setText('%f' % self.worker.yoff)
        else:
            self.Log.insertPlainText('Measurement aborted.')
            self.Log.moveCursor(QtGui.QTextCursor.End)

    def makelog(self):
        self.DoLogBook.setEnabled(False)
        if not self.noSave.isChecked():
            return
        filename=self.worker.filename
        fileroot=filename.split('.h5')[0]
        attach=[]
        for i in range(1,7):
            attach.append('%s_Fig%d.png' % (fileroot,i))
        self.worker.scan.save.writeLogbook(filename,attach,'QuadAlignment')
        self.Log.insertPlainText('Logbook entry created.')
        self.Log.moveCursor(QtGui.QTextCursor.End)


    #------------------------------------------
    # config event handler

    def saveconfig(self):
        quad=self.getQuad()
        dQ=str(float(self.ConfQuaddI.text()))
        dCor=str(float(self.ConfCordI.text()))
        self.layout.quadparameter[quad]['dQ']=dQ
        self.layout.quadparameter[quad]['dCor']=dCor
        self.layout.savePickle()

    def addCorrector(self):
        if len(str(self.SelQuad.currentText())) < 1:
            return
        quad=self.getQuad()
        if not quad in self.layout.quadparameter.keys():
            self.layout.quadparameter[quad]=self.layout.newEntry()
        cor=str(self.ConfCorList.currentItem().text())
        self.layout.quadparameter[quad]['Cor']=cor
        self.updateQuadrupole()
        
    def addBPMRef(self):
        if len(str(self.SelQuad.currentText())) < 1:
            return
        quad=self.getQuad()
        if not quad in self.layout.quadparameter.keys():
            self.layout.quadparameter[quad]=self.layout.newEntry()
        name=self.ConfBPMList.currentItem().text()
        bpm=str(name)
        self.layout.quadparameter[quad]['BPM']=bpm
        self.updateQuadrupole()

    def addBPMOrbit(self):
        if len(str(self.SelQuad.currentText())) < 1:
            return
        quad=self.getQuad()
        if not quad in self.layout.quadparameter.keys():
            self.layout.quadparameter[quad]=self.layout.newEntry()
        name=self.ConfBPMList.currentItem().text()
        bpm=str(name)
        if not bpm in self.layout.quadparameter[quad]['Orbit']:
            self.layout.quadparameter[quad]['Orbit'].append(bpm)
        self.updateQuadrupole()

    def removeBPM(self):
        quad=self.getQuad()
        ele=str(self.ConfBPMOrbit.currentItem().text())
        self.layout.quadparameter[quad]['Orbit'].remove(ele)
        self.updateQuadrupole()

    # ---------------------------------------
    # basic event handler

    def getQuad(self):
        return str(self.SelSection.currentText())+'-'+str(self.SelQuad.currentText())

    def updateSection(self):
        sec=str(self.SelSection.currentText())
        quads=self.layout.quads[sec]
        self.SelQuad.clear()
        for ele in quads:
            self.SelQuad.addItem(ele)


    def updateQuadrupole(self):
        quad=self.getQuad()
        self.ConfBPMList.clear()
        self.ConfCorList.clear()
        bpmlist=self.layout.getBPMlist(quad)
        for ele in bpmlist:
            self.ConfBPMList.addItem(ele)
        corlist=self.layout.getCorlist(quad)
        for ele in corlist:
            self.ConfCorList.addItem(ele)


        if not quad in self.layout.quadparameter.keys():
            quad='new'
        self.ConfBPMRef.setText(self.layout.quadparameter[quad]['BPM'])
        self.ConfCor.setText(self.layout.quadparameter[quad]['Cor'])
        self.ConfQuaddI.setText(self.layout.quadparameter[quad]['dQ'])
        self.ConfCordI.setText(self.layout.quadparameter[quad]['dCor'])
        self.ConfBPMOrbit.clear()
        for ele in self.layout.quadparameter[quad]['Orbit']:
            self.ConfBPMOrbit.addItem(ele)


    def setX(self):
        quad=self.getQuad()
        bpm=self.layout.quadparameter[quad]['BPM']
        pv='%s:X-OFFS' % bpm
        pvFB='%s:X-REF-FB' % bpm
        x=caget(pv)
        xnew=x+float(str(self.XOFFS.text()))
        xfb=caget(pvFB)
        xfbnew=xfb-float(str(self.XOFFS.text()))
        caput(pv,xnew)
        if self.CompFB.isChecked():
            caput(pvFB,xfbnew)

    def setY(self):
        quad=self.getQuad()
        bpm=self.layout.quadparameter[quad]['BPM']
        pv='%s:Y-OFFS' % bpm
        pvFB='%s:Y-REF-FB' % bpm
        x=caget(pv)
        xnew=x+float(str(self.YOFFS.text()))
        xfb=caget(pvFB)
        xfbnew=xfb-float(str(self.YOFFS.text()))
        caput(pv,xnew)
        if self.CompFB.isChecked():
            caput(pvFB,xfbnew)




#----------------------------
# plotting interface

    def initmpl(self):
        self.fig=Figure()
        self.axes=self.fig.add_subplot(111)
        self.canvas = FigureCanvas(self.fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar=NavigationToolbar(self.canvas,self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)




# --------------------------------
# Main routine

if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app=QtWidgets.QApplication(sys.argv)
    main=QuadAlignment()
    main.show()
    sys.exit(app.exec_())
