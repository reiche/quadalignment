
from time import sleep

import numpy as np
import sys
import re

from PyQt5 import QtGui,QtCore


# PV interface
from epics import PV,caput,caget


# Online Model specific, should become obsolete in the future.
sys.path.append('/sf/bd/applications/OnlineModel/current') 
sys.path.append('/sf/bd/applications/Svenstool/current')

from ScanCore import *

class QuadWorker(QtCore.QThread):

    signalEnd      = QtCore.pyqtSignal(bool)
    signalPlot     = QtCore.pyqtSignal(bool)
    signalProgress = QtCore.pyqtSignal(str)

    def __init__(self,parent=None):
        super(QuadWorker, self).__init__(parent)
        self.scan=ScanCore()
        self.scan.appInfo('QuadAlignment','v2.0.0','S. Reiche')
        self.scan.sigterm.connect(self.scanDone)
        self.abort=False
        self.save=False
        self.sensorlist=[]
        self.slope=None
        self.xpos=np.zeros(5)
        self.ypos=np.zeros(5)
        self.timeout=1.


    def initMeasurement(self,quadname,BPMname,CorrectorName,OrbitBPMs,dk1L,dCor,save=True):

        self.save=save
        nsam=10
        nstep=3
        actsv='%s:K1L-SET' % quadname
        actrb='%s:K1L-READ' % quadname
        act=caget(actsv)
        actmin=act-dk1L
        actmax=act+dk1L
        print(actmin,actmax)

        self.sensorlist.clear()
        if 'SAT' in BPMname:
            print('Adding Channel:',BPMname)
            self.sensorlist.append('%s:X2' % BPMname)
            self.sensorlist.append('%s:Y2' % BPMname)
        else:
            self.sensorlist.append('%s:X1' % BPMname)
            self.sensorlist.append('%s:Y1' % BPMname)
        self.sensorlist.append(actrb)
        for ele in OrbitBPMs:
            print('Adding Channels:', ele)
            if 'SAT' in ele:
                self.sensorlist.append('%s:X2' % ele)
                self.sensorlist.append('%s:Y2' % ele)
            else:
                self.sensorlist.append('%s:X1' % ele)
                self.sensorlist.append('%s:Y1' % ele)

        self.scan.setup(nsam,self.sensorlist,nstep=nstep,settletime=5,timeout=5)
        self.scan.actuator.add(actsv,min=actmin,max=actmax,nsteps=nstep)
#        self.scan.actuator.add(actsv,min=actmin,max=actmax,nsteps=nstep,readback=actrb,tol=0.05*dk1L)

        ndata=len(OrbitBPMs)*2
        self.slope=np.zeros((5,ndata,2))
        self.istep=0

        # correctors
        self.PVxcor  =PV('%s:I-SET' % CorrectorName.replace('COR','CRX'))
        self.PVxcorRB=PV('%s:I-READ' % CorrectorName.replace('COR','CRX'))
        self.PVycor  =PV('%s:I-SET' % CorrectorName.replace('COR','CRY'))
        self.PVycorRB=PV('%s:I-READ' % CorrectorName.replace('COR','CRY'))
        self.dCor=dCor
        return True

 
    def doAbort(self):
        self.scan.doAbort()
        self.abort=True

    def doEnd(self):
        if self.save:
            self.scan.save.closeFile()

        self.restoreSetting()
        if not self.abort:
            self.analyse()
        self.signalEnd.emit(self.abort)
        self.abort=False

###################################################
# actual measurement proceduressetting corrector

    def restoreSetting(self):
        self.signalProgress.emit('Restoring correctors...')
        self.PVxcor.put(self.Corx)
        self.PVycor.put(self.Cory)

    def readCurrentState(self):
        self.Corx=self.PVxcor.get()
        self.Cory=self.PVycor.get()


    def run(self):
        self.abort=False
        # do some prescan stuff
        self.readCurrentState()
        if self.save:
            self.filename=self.scan.save.openFile('QuadAlignment')

        self.istep=0
        steps=[[0,0],[1,0],[-1,0],[0,1],[0,-1]]
        for step in steps:
            self.setPosition(step)
            if self.abort:
                self.doEnd()
                return
        self.doEnd()


#####################################
# core measurement routine


    def setCorrector(self,x,y):

        svx=self.Corx+x*self.dCor
        svy=self.Cory+y*self.dCor

        print('putting Corrector to',svx,svy,'timeout',self.timeout)
        self.PVxcor.put(svx)
        self.PVycor.put(svy) 
        cont=True
        t1=datetime.datetime.now()

        while cont:
            rbx=self.PVxcorRB.get()
            rby=self.PVycorRB.get()
            t2=datetime.datetime.now()
            dt=(t2-t1).seconds
            dac=np.abs(svx-rbx)+np.abs(svy-rby)
            if dac<0.01 or dt > self.timeout:
                cont=False
            else:
                sleep(0.1)
        print('Setting correctors done')

    def scanDone(self):
        print('Individual Scan is done')
        self.scanIsRunning=False

    def setPosition(self,step):
        x=step[0]
        y=step[1]
        if x >0:
            xtag='+X'
        elif x < 0:
            xtag='-X'
        else:
            xtag='0'
        if y >0:
            ytag='+Y'
        elif y < 0:
            ytag='-Y'
        else:
            ytag='0'

        # setting the corrector
        msg='Measuring for (%s,%s)' % (xtag,ytag)
        self.signalProgress.emit(msg)        
        self.setCorrector(x,y)

    
        self.scanIsRunning=True
        self.scan.run()
        while self.scanIsRunning:
            sleep(0.5)
        if self.abort or self.scan.abort:
            return

        if self.save:
            self.scan.save.writeData(self.scan.data,self.istep+1)
        # do the data analysis

        # Reference BPM Position
        self.xpos[self.istep]=np.mean(self.scan.data[self.sensorlist[0]])
        self.ypos[self.istep]=np.mean(self.scan.data[self.sensorlist[1]])

        self.xdata=self.scan.data[self.sensorlist[2]].flatten()
        self.ydata=[]
        ibpm=0
        for i in range(3,len(self.sensorlist)): 
            y=self.scan.data[self.sensorlist[i]].flatten()
            pfit=np.polyfit(self.xdata,y,1)
            self.slope[self.istep,ibpm,0]=pfit[0]
            self.slope[self.istep,ibpm,1]=pfit[1]
            self.ydata.append(y)
            ibpm+=1
        self.istep+=1
        self.signalPlot.emit(True)

        return

    def analyse(self):

        nbpm=int((len(self.sensorlist)-3)/2.)
        print('Channels:',nbpm)

        x=[]
        x.append(self.xpos[2])
        x.append(self.xpos[0])
        x.append(self.xpos[1])
        self.xxscan=x
        self.xyscan=[]
        self.xfit=np.zeros((nbpm,2))

        # slope in x
        imax=0
        slope=None
        for i in range(nbpm):
            y=[]
            y.append(self.slope[2,2*i,0])
            y.append(self.slope[0,2*i,0])
            y.append(self.slope[1,2*i,0])
            pfit=np.polyfit(x,y,1)
            self.xyscan.append(y)
            self.xfit[i,0]=pfit[0]
            self.xfit[i,1]=pfit[1]
            if slope is None or abs(pfit[0]) > abs(slope[0]):
                slope=pfit
                imax=i
        self.xoff=-slope[1]/slope[0]


        # slope in y
        x=[]
        x.append(self.ypos[4])
        x.append(self.ypos[0])
        x.append(self.ypos[3])
        self.yxscan=x
        self.yyscan=[]
        self.yfit=np.zeros((nbpm,2))

        imax=0
        slope=None
        for i in range(nbpm):
            y=[]
            y.append(self.slope[4,2*i+1,0])
            y.append(self.slope[0,2*i+1,0])
            y.append(self.slope[3,2*i+1,0])
            pfit=np.polyfit(x,y,1)
            self.yyscan.append(y)
            self.yfit[i,0]=pfit[0]
            self.yfit[i,1]=pfit[1]
            if slope is None or abs(pfit[0]) > abs(slope[0]):
                slope=pfit
                imax=i
        self.yoff=-slope[1]/slope[0]
        


        









