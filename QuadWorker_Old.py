
import time

import numpy as np
import sys
import re

from PyQt5 import QtGui,QtCore


# PV interface
from epics import PV,caput,caget


# Online Model specific, should become obsolete in the future.
sys.path.append('/sf/bd/applications/OnlineModel/current') 
sys.path.append('/sf/bd/applications/Svenstool/current')

from SaveCore import *
from SnapShotCore import *



class QuadWorker(QtCore.QThread):

    signalStatus   = QtCore.pyqtSignal(bool)
    signalProgress = QtCore.pyqtSignal(str)

    def __init__(self,parent=None):
        super(QuadWorker, self).__init__(parent)
        
        self.abort=False
        self.PVBPM=[]
        self.measX=[]
        self.measY=[]
        self.measX2=[]
        self.measY2=[]
        self.pfitx=None
        self.pfity=None  
        self.plotidx=0
        self.canvas=None
        self.axes=None
        # holding the data 
        self.sensor=[]
        self.actuator=[]
        self.results=[]
        self.sensave={}
        self.actsave={}
        self.derived={}
        self.config={'scantype':'LineScan','steps':2,'samples':10}
        self.save=SaveCore('QuadAlignment','S. Reiche','v1.0.0')
        self.snap=SnapShotCore()
        self.filename=''
        self.plotnames=[]
        self.scanrun=0
        self.noSave=False
        self.xoff=0
        self.yoff=0



    def run(self):

        self.plotnames.clear()
        self.abort=False
        self.signalProgress.emit('Measurement started...')
        if not self.noSave:
            self.filename=self.save.openFile('Quad Alignment')

        self.snap.getSnap()
        if not self.noSave:
            self.save.writeSnapShot(self.snap.snap)
            self.signalProgress.emit('Snapshot taken...')

        self.readCurrentState()
        
        self.scanrun=1
        if self.setPosition(0.,0.):
            self.endRun()
            return
        if not self.noSave:
            self.save.writeData(self.config, self.sensave, self.actsave, self.derived, self.scanrun)
            self.saveplot(self.scanrun)
        self.scanrun=self.scanrun+1

        if self.setPosition(1.,0.):
            self.endRun()
            return
        if not self.noSave:
            self.save.writeData(self.config, self.sensave, self.actsave, self.derived, self.scanrun)
            self.saveplot(self.scanrun)
        self.scanrun=self.scanrun+1

        if self.setPosition(-1.,0.):
            self.endRun()
            return
        if not self.noSave:
            self.save.writeData(self.config, self.sensave, self.actsave, self.derived, self.scanrun)
            self.saveplot(self.scanrun)
        self.scanrun=self.scanrun+1

        if self.setPosition(0.,1):
            self.endRun()
            return
        if not self.noSave:
            self.save.writeData(self.config, self.sensave, self.actsave, self.derived, self.scanrun)
            self.saveplot(self.scanrun)
        self.scanrun=self.scanrun+1

        self.setPosition(0.,-1)
        if not self.noSave:
            self.save.writeData(self.config, self.sensave, self.actsave, self.derived, self.scanrun)
            self.saveplot(self.scanrun)


# analysis
        x=[]
        x.append(self.results[2,0])
        x.append(self.results[0,0])
        x.append(self.results[1,0])
        slope=None
        imax=0
        # plot results
        self.axes.clear()
        color=['b','r','g','c','y']
        for i in range(5):
            y=[]
            y.append(self.results[2,2*i+2])
            y.append(self.results[0,2*i+2])
            y.append(self.results[1,2*i+2])
            pfit=np.polyfit(x,y,1)
            if slope is None or abs(pfit[0]) > abs(slope[0]):
                slope=pfit
                imax=i
            self.axes.plot(x,y,color[i])
#        self.axes.set_title('X-Alignment: %s - %s' % (self.PVBPM[0].pvname.split(':')[0],self.PVquad.pvname.split(':')[0]))
#        self.axes.set_xlabel('BPM-Reading in X (mm)')
#        self.axes.set_ylabel('Orbit-Sensitivity')
#        self.canvas.draw()
#        if not self.noSave:
#            self.saveplot(6)
        self.xoff=-slope[1]/slope[0]
        self.axes.plot([self.xoff,self.xoff],[-1,1],color[imax])
        

        slope=None
        imax=0

        x=[]
        x.append(self.results[4,1])
        x.append(self.results[0,1])
        x.append(self.results[3,1])

        # plot results
#        self.axes.clear()
#        color=['b','r','g','c','y']
        for i in range(5):
            y=[]
            y.append(self.results[4,2*i+3])
            y.append(self.results[0,2*i+3])
            y.append(self.results[3,2*i+3])
            pfit=np.polyfit(x,y,1)
            if slope is None or abs(pfit[0]) > abs(slope[0]):
                slope=pfit
                imax=i
            self.axes.plot(x,y,color[i]+'--')
        self.yoff=-slope[1]/slope[0]
        self.axes.plot([self.yoff,self.yoff],[-1,1],color[imax]+'--')


        self.axes.set_title('Alignment: %s - %s' % (self.PVBPM[0].pvname.split(':')[0],self.PVquad.pvname.split(':')[0]))
        self.axes.set_xlabel('BPM-Reading in x/Y (mm)')
        self.axes.set_ylabel('Orbit-Sensitivity')
        self.canvas.draw()
        if not self.noSave:
            self.saveplot(6)







        self.endRun()
        


    def endRun(self):
        self.restoreSetting()
        if not self.noSave:
            self.save.closeFile()
        
        if not self.abort:
            self.signalProgress.emit('Generating Logbook entry...')
            if not self.noSave:
                self.save.writeLogbook(self.filename,self.plotnames)
        else:
            os.remove(self.filename)   
        self.signalStatus.emit(not self.abort)

    def stop(self):
        self.abort=True

    def saveplot(self,idx):
        plotfile=self.filename.split('.h5')[0]+('_%d.png' % idx)
        self.canvas.print_figure(plotfile)
        self.plotnames.append(plotfile)
        


#################################################
#  defining the measurements

    def initMeasurement(self,quadname,BPMname,CorrectorName,OrbitBPMs,dk1L,dCor,canvas,axes,nosave):
        self.PVquad  =PV('%s:K1L-SET' % quadname)
        self.PVquadRB=PV('%s:K1L-READ' % quadname)
        self.PVBPM.clear()        
        self.PVBPM.append(PV('%s:X1-RT' % BPMname))
        self.PVBPM.append(PV('%s:X1-VALID-RT' % BPMname))
        self.PVBPM.append(PV('%s:Y1-RT' % BPMname))
        self.PVBPM.append(PV('%s:Y1-VALID-RT' % BPMname))
        for ele in OrbitBPMs:
            self.PVBPM.append(PV('%s:X1-RT' % ele))
            self.PVBPM.append(PV('%s:X1-VALID-RT' % ele))
            self.PVBPM.append(PV('%s:Y1-RT' % ele))
            self.PVBPM.append(PV('%s:Y1-VALID-RT' % ele))
        self.PVxcor  =PV('%s:I-SET' % CorrectorName.replace('COR','CRX'))
        self.PVxcorRB=PV('%s:I-GET' % CorrectorName.replace('COR','CRX'))
        self.PVycor  =PV('%s:I-SET' % CorrectorName.replace('COR','CRY'))
        self.PVycorRB=PV('%s:I-GET' % CorrectorName.replace('COR','CRY'))
        self.dk1L=dk1L
        self.dCor=dCor
        self.noSave=nosave

        count=len(self.PVBPM)
        self.Nstep=3
        self.Nsample=10
        self.actuator=np.ndarray((self.Nstep,))
        self.sensor=np.ndarray((self.Nstep,count,self.Nsample,))
        self.results=np.ndarray((5,int(count/2),))

        self.canvas=canvas
        self.axes=axes

###################################################
# actual measurement procedures

    def restoreSetting(self):
        self.signalProgress.emit('Restoring settings...')
        self.PVquad.put(self.k1L)
        self.PVxcor.put(self.Corx)
        self.PVycor.put(self.Cory)
        time.sleep(1)


    def readCurrentState(self):
        self.k1L=self.PVquad.get()
        self.Corx=self.PVxcor.get()
        self.Cory=self.PVycor.get()

#####################################
# core measurement routine

    def setPosition(self,x,y):
        if x >0:
            xtag='+X'
        elif x < 0:
            xtag='-X'
        else:
            xtag='0'
        if y >0:
            ytag='+Y'
        elif y < 0:
            ytag='-Y'
        else:
            ytag='0'

        msg='Measuring for (%s,%s)' % (xtag,ytag)
        self.signalProgress.emit(msg)


        self.PVxcor.put(self.Corx+x*self.dCor)
        self.PVycor.put(self.Cory+y*self.dCor)
        time.sleep(2)


        # set position        
        self.setQuad()
        if (self.abort):
            return

        self.actsave[self.PVquad.pvname]=self.actuator
        for i in range(len(self.PVBPM)):
            self.sensave[self.PVBPM[i].pvname]=self.sensor[:,i,:]


        # plot results
        self.axes.clear()
        color=['b','r','g','c','y']

        pvcenter=self.PVBPM[0].pvname.split(':')[0]
        self.derived[pvcenter]={}
        self.derived[pvcenter]['XCENTER']=np.mean(self.sensor[:,0,:])
        self.derived[pvcenter]['YCENTER']=np.mean(self.sensor[:,2,:])
        self.results[self.scanrun-1,0]=self.derived[pvcenter]['XCENTER']
        self.results[self.scanrun-1,1]=self.derived[pvcenter]['YCENTER']
        for i in range(5):
            pvfull=self.PVBPM[4+4*i].pvname.split(':')
            pv=pvfull[0]
            self.derived[pv]={}
            for j in range(2):            
                y   =np.mean(self.sensor[:,4+4*i+2*j,:],axis=1)
                yerr=np.std(self.sensor[:,4+4*i+2*j,:],axis=1)
                pfit=np.polyfit(self.actuator,y,1)
                if (j%2) is 0:
                    self.derived[pv]['XSLOPE']=pfit[0]
                    self.results[self.scanrun-1,i*2+2]=self.derived[pv]['XSLOPE']                    
                    colorfit=color[i]
                else:
                    self.derived[pv]['YSLOPE']=pfit[0]
                    self.results[self.scanrun-1,i*2+3]=self.derived[pv]['YSLOPE']                    
                    colorfit=color[i]+'--'
                self.axes.errorbar(self.actuator,y,yerr=yerr,fmt='o',color=color[i])
                yfit=np.polyval(pfit,self.actuator)
                self.axes.plot(self.actuator,yfit,colorfit)

        self.axes.set_title(msg)
        self.axes.set_xlabel('k1L')
        self.axes.set_ylabel('BPM-Reading')
        self.canvas.draw()
        

        return self.abort


    def setQuad(self):        

        if self.measureBPM(1):    # measure at nominal quad strength
            return
        self.actuator[1]=self.PVquad.get()


        self.PVquad.put(self.k1L+self.dk1L)
        while abs(self.PVquad.get()-self.PVquadRB.get()) > 0.001:
            if (self.abort):
                return
            time.sleep(0.01)
        time.sleep(0.5)
        if self.measureBPM(2):    # measure at increased quadstrength
            return
        self.actuator[2]=self.PVquad.get()

 


        self.PVquad.put(self.k1L-self.dk1L)
        while abs(self.PVquad.get()-self.PVquadRB.get()) > 0.001:
            if (self.abort):
                return
            time.sleep(0.01)
        time.sleep(0.5)
        self.measureBPM(0)       # measure at decreased quad strength        
        self.actuator[0]=self.PVquad.get()
 

        self.PVquad.put(self.k1L)
        while abs(self.PVquad.get()-self.PVquadRB.get()) > 0.001:
            if (self.abort):
                return
            time.sleep(0.01)
        time.sleep(0.5)    
        print('restoring')    
        return

    def measureBPM(self,idx):
        
                                 
        nsen=self.sensor.shape[1]                                 
        lastX=np.ndarray((int(nsen/2),))*0                         

        N=int(0);
        while N < self.Nsample:
           if self.abort:    # check for abort
               return self.abort
           res=[ele.get() for ele in self.PVBPM]
           valid=sum(res[1::2])
           if valid is len(res[1::2]):
               x=np.array(res[0::2])
               if not np.array_equal(x,lastX):
                   self.sensor[idx,:,N]=res                  
                   lastX=x
                   N=N+1
               else:
                   time.sleep(0.005)
           else:
               time.sleep(0.005)
        return self.abort








